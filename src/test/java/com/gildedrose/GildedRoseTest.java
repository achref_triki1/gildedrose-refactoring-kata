package com.gildedrose;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;

/**
 * @author Achref TRIKI
 * @email achref.triki@gmail.com
 *
 */
class GildedRoseTest {

	@DisplayName("At the end of each day our system lowers both values for every item ({+5 Dexterity Vest} in our case)")
	@Test
	void TestNormalItemQualityIfSellBeforeDatePassed() {
		Item[] items = new Item[] { new Item("+5 Dexterity Vest", 2, 3) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(1, app.items[0].sellIn);
		assertEquals(2, app.items[0].quality);
	}

	@DisplayName("Once the sell by date has passed, Quality degrades twice as fast")
	@Test
	void TestNormalItemQualityIfSellByDatePassed() {
		Item[] items = new Item[] { new Item("+5 Dexterity Vest", -1, 3) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(-2, app.items[0].sellIn);
		// Quality degrades twice as fast
		assertEquals(1, app.items[0].quality);
	}

	@DisplayName("The Quality of an item is never negative in both cases : 1. sell by date has not yet passed & 2.sell by date has passed) ")
	@ParameterizedTest(name = "A negative value for quality is not permitted.")
	@ValueSource(ints = {0,-1})
	void TestItemQualityIsNeverNegative(int sellIn) {

		Item[] items = new Item[] { 
									new Item("+5 Dexterity Vest", sellIn, 0), 
									new Item("Aged Brie", sellIn, 0),
									new Item("Elixir of the Mongoose", sellIn, 0), 
									new Item("Backstage passes to a TAFKAL80ETC concert", sellIn, 0),
									new Item("Conjured Mana Cake", sellIn, 0) 
		};

		GildedRose app = new GildedRose(items);
		app.updateQuality();

		assertAll("app", 
					() -> assertTrue(app.items[0].quality >= 0), 
					() -> assertTrue(app.items[1].quality >= 0),
					() -> assertTrue(app.items[2].quality >= 0), 
					() -> assertTrue(app.items[3].quality >= 0),
					() -> assertTrue(app.items[4].quality >= 0)
		);
	}

	

	@DisplayName("Aged Brie actually increases in Quality the older it gets")
	@Test
	void TestAgedBrieQualityIncreaseByTime() {
		Item[] items = new Item[] { new Item("Aged Brie", -3, 6) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertTrue(app.items[0].quality > 6);
	}

	@DisplayName("The Quality of an item is never more than 50 (Except Sulfuras)")
	@ParameterizedTest(name = "A Quality value > 50 is not permitted.")
	@ValueSource(strings = { "+5 Dexterity Vest", "Aged Brie", "Elixir of the Mongoose",
			"Backstage passes to a TAFKAL80ETC concert", "Conjured Mana Cake" })
	void TestQualityOfItemNeverExceed50(String item) {
		Item[] items = new Item[] { new Item(item, -3, 50) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(-4, app.items[0].sellIn);
		assertTrue(app.items[0].quality <= 50);
	}

	@DisplayName("Backstage passes Quality increase process")
	@ParameterizedTest(name = "Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but Quality drops to 0 after the concert")
	@MethodSource("daysAndexceptedValues")
	void TestBackstageQualityIncreaseProcess(List<Integer> input, int output) {
		Random r = new Random();
		int minVal = input.get(0);
		int maxVal = input.get(1);
		// Random.nextInt() Returns a pseudo random, uniformly distributed int value
		// between 0(inclusive) and the specified value (exclusive)
		int sellIn = r.nextInt(maxVal - minVal) + minVal;

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", sellIn, 2) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(2 + output, app.items[0].quality);

	}

	static Stream<Arguments> daysAndexceptedValues() {
		return Stream.of(Arguments.arguments(Arrays.asList(1, 6), 3), Arguments.arguments(Arrays.asList(6, 11), 2),
				Arguments.arguments(Arrays.asList(11, 1000), 1));

	}

	@DisplayName("Backstage passes Quality drops to 0 after the concert")
	@Test
	void TestBackstageQualityAfterConcert() {
		Random r = new Random();

		int quality = r.nextInt(999999999) + 1;
		// sellIn has a value between -999999999 and -1 (sell by date has passed)
		int sellIn = -(quality);
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, app.items[0].quality);

	}
	
	@DisplayName("Conjured items degrade in Quality twice as fast as normal items")
	@Test
	void TestConjuredItemQualityIfSellBeforeDatePassed() {
		Item[] items = new Item[] { new Item("Conjured Mana Cake", 2, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(1, app.items[0].sellIn);
		assertEquals(3, app.items[0].quality);
	}

	@DisplayName("Once the sell by date has passed, Conjured items Quality degrades twice as fast as Normal items Quality")
	@Test
	void TestConjuredItemQualityIfSellByDatePassed() {
		Item[] items = new Item[] { new Item("Conjured Mana Cake", -1, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(-2, app.items[0].sellIn);
		// Quality degrades twice as fast as normal item 
		assertEquals(1, app.items[0].quality);
	}
	
	@DisplayName("Sulfuras is a legendary item : never has to be sold or decreases in Quality(80)")
	@Test
	void TestSulfateQualityunchanged() {
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -1, 80) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(-1, app.items[0].sellIn);
		assertEquals(80, app.items[0].quality);
	}

}
